function New-VagPsSession {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,
        Position=0,
        ValueFromPipeline=$true,
        ParameterSetName="Name")]
        [String]
        $Name,
        
        # Parameter help description
        [Parameter(Mandatory=$true,
        ValueFromPipelineByPropertyName=$true,
        ParameterSetName="ID")]
        [String]
        $ID,

        [Parameter(Mandatory=$false)]
        [String]
        $SessionName,

        [Parameter(Mandatory=$true)]
        [pscredential]
        $Credential
    )
    begin {
        $globalStatus = Get-VagGlobalStatus
    }
    
    process {
        $vagVM = $null
        if ($psCmdlet.ParameterSetName -eq "Name") {
            $vagVM =  $globalStatus | Where-Object Name -like $name   
        }else {
            $vagVM = $globalStatus | Where-Object ID -like $ID   
        }
        if ($vagVM.provider -eq 'virtualbox') {
            $winrmPort = Get-VagVMPort -name $vagVM.name | Where-Object Name -EQ "winrm"
            New-PsSession -ComputerName 127.0.0.1 -Port $winrmPort.hostport -Credential $credential -name $SessionName
        }    
    }
    
    end {

    }
}