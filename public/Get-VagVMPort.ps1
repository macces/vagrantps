function Get-VagVMPort {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $true,
            Position = 0,
            ParameterSetName = "Name")]
        [String[]]
        $Name,
        # Parameter help description
        [Parameter(Mandatory = $true,
            ValueFromPipelineByPropertyName = $true,
            ParameterSetName = "ID")]
        [String[]]
        $ID
    )
    BEGIN{

    }
    process{
        
        $upMachineInfos = @()
        if ($psCmdlet.ParameterSetName -eq "Name") {
            foreach ($_name in $Name) {
                $upMachineInfos += get-VagVMInformation -Name $_name 
            }  
        }else {
            foreach ($_ID in $Id) {
                $upMachineInfos += get-VagVMInformation -id $_ID
            }    
        }
        
        foreach ($upMachineInfo in $upMachineInfos) {
            
            if ($upMachineInfo.provider -eq "virtualbox") {
                $vagMachineSettingsPath = $upMachineInfo.local_data_path
                "machines", $upMachineInfo.name, $upMachineInfo.provider | ForEach-Object {
                    $vagMachineSettingsPath = Join-Path -Path $vagMachineSettingsPath -ChildPath $_
                }
                #Get VM Guid - only tested with VirtualBox
                $VmGuid = ((Get-Content (Join-Path -Path $vagMachineSettingsPath -ChildPath "action_provision")) -split ":")[-1]
                $VBinfo = VBoxManage showvminfo $VmGuid
                $hash = @()
                #Information about Networksettings can be found in the line "NIC x". this line will tell you what kind of network the guest uses. It will not tell you the IP Adress. (NAT = 127.0.0.1)
                #NIC 1:           MAC: 08002796A85F, Attachment: NAT, Cable connected: on, Trace: off (file: none), Type: 82540EM, Reported speed: 0 Mbps, Boot priority: 0, Promisc Policy: deny, Bandwidth group: none
                # $keyVal[0] -match '^NIC\s\d$' will find the NIC Settings
                foreach ($item in $VBinfo ) {
                    $keyVal = $item -split ": " 
                    
                    if ($keyVal[0] -and $keyVal[1] ) {
                        $hash += @{
                            $keyVal[0] = $($keyVal[1].Trim())
                        }
                    }
                }
                $nicRule = ($hash | Where-Object {$_.keys -like "NIC*Rule*"} | Select-Object -expandproperty Values) 
                $ports = @()
                
                foreach ($_NicRule in $nicRule) {
                    $_NicRule = ($_NicRule -split ",").TrimEnd().TrimStart()
                    #$_NicRule is a Array of Hashtables
                    $_NicRule = $_NicRule | ConvertFrom-StringData 
                    $props = [ordered]@{MachineName = $upMachineInfo.name
                        MachineID                   = $upMachineInfo.ID
                        Name                        = $_NicRule.name
                        Protocol                    = $_NicRule.protocol
                        HostPort                    = $_NicRule.'host port'
                        GuestPort                   = $_NicRule.'guest port'
                    }
                    
                    $ports += New-Object -TypeName psobject -Property $props
                    
                }
                $ports
            }
        }
    }
    END{

    }
}