function Remove-VagVM {
    [CmdletBinding(SupportsShouldProcess,
    ConfirmImpact = 'high')]
    param(
        [Parameter(Mandatory=$true,
        Position=0,
        ParameterSetName="Name")]
        [String[]]
        $Name,
        # Parameter help description
        [Parameter(Mandatory=$true,
        ValueFromPipelineByPropertyName=$true,
        ParameterSetName="ID")]
        [String[]]
        $ID,
        [Parameter()]
        [switch]
        $Force
    )
    begin {
        
    }
    
    process {
        $upMachineInfos = @()
        if ($psCmdlet.ParameterSetName -eq "Name") {
            foreach ($_name in $Name) {
                $upMachineInfos += get-VagVMInformation -Name $name 
            }  
        }else {
            foreach ($_ID in $Id) {
                $upMachineInfos += get-VagVMInformation -id $ID
            }    
        }
        foreach ($upMachineInfo in $upMachineInfos) {
            if ($Force -or $pscmdlet.ShouldProcess("$($upMachineInfo.name) in $($upMachineInfo.vagrantfile_path)", "vagrant destroy")) {
                $result = vagrant.exe destroy $upMachineInfo.ID -f
                if (!$result -match 'Destroying VM and associated drives...') {
                    throw "Error: machine couldn't be detroy throw `n $result"
                }else {
                    "$($upMachineInfo.name) in $($upMachineInfo.vagrantfile_path) was sucessfully detroyed"
                }
             }else{
                 
             }  

         
           
        }
    }
    
    end {

    }
}