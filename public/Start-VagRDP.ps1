function Start-VagRdp {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,
        Position=0,
        ParameterSetName="Name")]
        [String[]]
        $Name,
        # Parameter help description
        [Parameter(Mandatory=$true,
        ValueFromPipelineByPropertyName=$true,
        ParameterSetName="ID")]
        [String[]]
        $ID
    )
    begin {
        $globalStatus = Get-VagGlobalStatus
    }
    
    process {
        if ($psCmdlet.ParameterSetName -eq "Name") {
            foreach ($_name in $Name) {
                $vagVM =  $globalStatus | Where-Object Name -like $_name 
                if ($vagVM.provider -eq 'virtualbox') {
                    $rdpPort = Get-VagVMPort -name $vagVM.name | Where-Object Name -EQ "rdp"
                    Connect-Mstsc -ComputerName "127.0.0.1:$($rdpPort.hostport)"
                }
                

            }  
        }else {
            foreach ($_ID in $Id) {
                $vagVM =  $globalStatus | Where-Object ID -like $_ID
                if ($vagVM.provider -eq 'virtualbox') {
                    $rdpPort = Get-VagVMPort -name $vagVM.name | Where-Object Name -EQ "rdp"
                    Connect-Mstsc -ComputerName "127.0.0.1:$($rdpPort.hostport)"
                }
            }    
        }
    }
    
    end {

    }
}

