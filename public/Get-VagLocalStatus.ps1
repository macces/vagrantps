function Get-VagLocalStatus {
    [CmdletBinding()]
    Param (
        # Parameter help description
        [Parameter(Mandatory = $false)]
        [String]
        $Path = $null


    )
    if ([string]::IsNullOrEmpty($path)) {
        Get-VagGlobalStatus | Where-Object { $_.vagrantfile_path -eq (Get-Location).Path}
    }else {
        Get-VagGlobalStatus | Where-Object { $_.vagrantfile_path -eq $Path}
    }
    
}
