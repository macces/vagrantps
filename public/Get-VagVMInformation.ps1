function Get-VagVMInformation {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,
        Position=0,
        ValueFromPipeline=$true,
        
        ParameterSetName="Name")]
        [String[]]
        $Name,
        # Parameter help description
        [Parameter(Mandatory=$true,
        ValueFromPipelineByPropertyName=$true,
        ParameterSetName="ID")]
        [String[]]
        $ID
    )
    begin {
        $globalStatus = Get-VagGlobalStatus
    }
    
    process {
        if ($psCmdlet.ParameterSetName -eq "Name") {
            foreach ($_name in $Name) {
                $globalStatus | Where-Object name -like $_name
            }  
        }else {
            foreach ($_ID in $Id) {
                $globalStatus | Where-Object ID -like $_ID
            }    
        }
    }
    
    end {

    }
}