function Get-VagGlobalStatus {
    [CmdletBinding()]
    Param ()
    # this is the file which stores the global index - the path is relative to
    # the Vagrant home.
    $vagIndexPath = Get-VagEnvironmentHome
    Write-Verbose "Using Vagrant home path '$vagIndexPath'"

    "data", "machine-index", "index" | ForEach-Object {
        $vagIndexPath = Join-Path -Path $vagIndexPath -ChildPath $_
    }
    Write-Verbose "Using Vagrant global index status path '$vagIndexPath'"

    if (Test-Path -Path $vagIndexPath -PathType Leaf) {
        # the index file is simply JSON so lets convert it
        $vagMachine = Get-Content -Path $vagIndexPath | ConvertFrom-Json
        # - get a list of the machine names (contained in
        # machines.psobject.properties.name)
        # - use that machine name list to access the underlying machine objects
        # and add the machine name as the id to that object
        # - return the machine object
        $status = @() 
        foreach ($machine in $vagmachine.machines.PSObject.Properties) {
            $obj = New-Object -TypeName PSObject
            $obj | Add-Member -MemberType NoteProperty -Name 'GUID' -Value $machine.name
            $obj | Add-Member -MemberType NoteProperty -Name 'ID' -Value ($machine.name).Substring(0,8) 
            foreach ($machinedata in ($machine.PSObject.Properties| Where-Object name -eq value).value.psobject.properties) {
                if ($machinedata.name -like "*path") {
                    $obj | Add-Member -MemberType NoteProperty -Name $machinedata.name -value ($machinedata.value.Replace('/', '\') )
                }else{
                    $obj | Add-Member -MemberType NoteProperty -Name $machinedata.name -value $machinedata.value
                }
            }
            $status += $obj 
        }

        Write-Verbose "Found $(@($status).count) running Vagrant machines."
    }
    else {
        Write-Verbose 'Found no running Vagrant machines.'
        $status = $null
    }

    $status
}




